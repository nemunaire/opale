#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import statistics
import sys

from opale.carte import *
from opale.game import Game

max_importance = None
mean_importance = {
    Chasseresse: 7,
    DragonCorail: 7,
    Pearl: 7,
    Witch: 7,
    DragonPetrified: 7,
    Golem: 7,
    Guardian: 7,
    Horser: 7,
    City: 7,
}
values_importance = {
    Chasseresse: [7],
    DragonCorail: [7],
    Pearl: [7],
    Witch: [7],
    DragonPetrified: [7],
    Golem: [7],
    Guardian: [7],
    Horser: [7],
    City: [7],
}
borne_min_importance = {
    Chasseresse: 14,
    DragonCorail: 14,
    Pearl: 14,
    Witch: 14,
    DragonPetrified: 14,
    Golem: 14,
    Guardian: 14,
    Horser: 14,
    City: 14,
}
borne_max_importance = {
    Chasseresse: 0,
    DragonCorail: 0,
    Pearl: 0,
    Witch: 0,
    DragonPetrified: 0,
    Golem: 0,
    Guardian: 0,
    Horser: 0,
    City: 0,
}

NB_ROUND = 999

seed_range = random.randrange(1000000000)
dum_range = seed_range + min(NB_ROUND/10, 99)
for seed in range(seed_range, seed_range + NB_ROUND):
    sys.stdout.write("\r%d/%d - %s  " % (seed - seed_range, NB_ROUND, {k.__name__: int(mean_importance[k]*100)/100 for k in mean_importance.keys()}))
    max_score_diff = 0
    for i in range(100):
        random.seed(seed)
        game = Game(None, None)

        random.seed()
        if seed > dum_range and i > 50:
            newImportance = {
                Chasseresse: random.randint(int(borne_min_importance[Chasseresse]),int(borne_max_importance[Chasseresse]+1)),
                DragonCorail: random.randint(int(borne_min_importance[DragonCorail]),int(borne_max_importance[DragonCorail]+1)),
                Pearl: random.randint(int(borne_min_importance[Pearl]),int(borne_max_importance[Pearl]+1)),
                Witch: random.randint(int(borne_min_importance[Witch]),int(borne_max_importance[Witch]+1)),
                DragonPetrified: random.randint(int(borne_min_importance[DragonPetrified]),int(borne_max_importance[DragonPetrified]+1)),
                Golem: random.randint(int(borne_min_importance[Golem]),int(borne_max_importance[Golem]+1)),
                Guardian: random.randint(int(borne_min_importance[Guardian]),int(borne_max_importance[Guardian]+1)),
                Horser: random.randint(int(borne_min_importance[Horser]),int(borne_max_importance[Horser]+1)),
                City: random.randint(int(borne_min_importance[City]),int(borne_max_importance[City]+1)),
            }
            z = i % 10
            if z == 0:
                newImportance[Chasseresse] = random.randint(0,14)
            elif z == 1:
                newImportance[DragonCorail] = random.randint(0,14)
            elif z == 2:
                newImportance[Pearl] = random.randint(0,14)
            elif z == 3:
                newImportance[Witch] = random.randint(0,14)
            elif z == 4:
                newImportance[DragonPetrified] = random.randint(0,14)
            elif z == 5:
                newImportance[Golem] = random.randint(0,14)
            elif z == 6:
                newImportance[Guardian] = random.randint(0,14)
            elif z == 7:
                newImportance[Horser] = random.randint(0,14)
            elif z == 8:
                newImportance[City] = random.randint(0,14)
        else:
            newImportance = {
                Chasseresse: random.randint(0,14),
                DragonCorail: random.randint(0,14),
                Pearl: random.randint(0,14),
                Witch: random.randint(0,14),
                DragonPetrified: random.randint(0,14),
                Golem: random.randint(0,14),
                Guardian: random.randint(0,14),
                Horser: random.randint(0,14),
                City: random.randint(0,14),
            }

        game.player1.importance = lambda: newImportance
        game.player2.base_importance = lambda t: mean_importance[t]

        def play_turn():
            return game.current_player.play_turn(game.play_round)

        while not game.isFinished():
            play_turn()
        play_turn()

        if game.player1.get_score() > game.player2.get_score():
            #print("Fight %d.%d: \033[93mP1\033[0m: \033[1m%d\033[0m pts vs. \033[93mP2\033[0m: \033[1m%d\033[0m pts" % (seed-seed_range, i, game.player1.get_score(), game.player2.get_score()))
            if game.player1.get_score() - game.player2.get_score() > max_score_diff:
                max_score_diff = game.player1.get_score() - game.player2.get_score()
                max_importance = newImportance
    for t in mean_importance.keys():
        values_importance[t].append(max_importance[t])
        mean_importance[t] = statistics.mean(values_importance[t])
        if seed >= dum_range:
            if borne_min_importance[t] > mean_importance[t]:
                borne_min_importance[t] = mean_importance[t]
            if borne_max_importance[t] < mean_importance[t]:
                borne_max_importance[t] = mean_importance[t]

print()
print("\nMax importance (diff=%d): %s" % (max_score_diff, max_importance))
print("\nBornes: %s" % ({t: "%s - %s" % (borne_min_importance[t], borne_max_importance[t]) for t in mean_importance}))
print("\nMean importance: %s" % (mean_importance))

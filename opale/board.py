#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 22:23:56 2020

@author: AlexandreMouchel
"""

from opale.carte import *

class Board():

    def __init__(self):
        self.chasseresse = []
        self.dragonCorail = []
        self.pearl = []
        self.witch = []
        self.dragonPetrified = []
        self.golem = []
        self.guardian = []
        self.horser = []
        self.city = []
        self.underTheBoard = []
        self.roundCity = 0

    def __repr__(self):
        return """
        \033[1;%dm%d\033[0m -> \033[1;%dm%d\033[0m <- \033[1;%dm%d\033[0m -> \033[1;%dm%d\033[0m
        ^         ^
        |  \033[%dm%d\033[0m     / \\
        \033[1;%dm%d\033[0m      \033[1;%dm%d\033[0m --> \033[1;%dm%d\033[0m  \033[1;%dm%d\033[0m
           \033[1;%dm%d\033[0m
""" % (DragonCorail.color, len(self.dragonCorail), Pearl.color, len(self.pearl), Witch.color, len(self.witch), DragonPetrified.color, len(self.dragonPetrified), City.color, len(self.city), Chasseresse.color, len(self.chasseresse), Horser.color, len(self.horser), Golem.color, len(self.golem), Guardian.color, len(self.guardian), 97, len(self.underTheBoard))

    def isFinished(self):
        return self.roundCity >= 3

    def play_card(self, *cards):
        ret = []

        for card in cards:
            if type(card) == Chasseresse:
                self.chasseresse.append(card)

                if len(self.chasseresse) >= 3:
                    self.underTheBoard += self.chasseresse
                    self.chasseresse = []

                    ret += self.dragonCorail
                    self.dragonCorail = []

            elif type(card) == DragonCorail:
                self.dragonCorail.append(card)

                ret += self.pearl
                self.pearl = []

            elif type(card) == Pearl:
                self.pearl.append(card)

            elif type(card) == Witch:
                self.witch.append(card)

                ret += self.pearl
                self.pearl = []

                ret += self.dragonPetrified
                self.dragonPetrified = []

            elif type(card) == DragonPetrified:
                self.dragonPetrified.append(card)

            elif type(card) == Golem:
                self.golem.append(card)

                ret += self.witch
                self.witch = []

            elif type(card) == Guardian:
                self.guardian.append(card)

                if len(self.guardian) >= 4:
                    ret += self.guardian
                    self.guardian = []

            elif type(card) == Horser:
                self.horser.append(card)

                if len(self.horser) >= 2:
                    ret += self.witch
                    ret += self.golem
                    self.witch = []
                    self.golem = []

                    self.underTheBoard += self.horser
                    self.horser = []

            elif type(card) == City:
                self.city.append(card)

                if len(self.city) >= 3:
                    self.roundCity += 1
                    self.city = []

                    ret += self.underTheBoard
                    self.underTheBoard = []

            else:
                raise Exception("Unknown card type " + type(card).__name__)

        return ret


    def try_cards(self, *cards):
        maintype = type(cards[0])
        for card in cards:
            if maintype != type(card):
                raise Exception("You can only play one card type at a time.")

        if maintype == Chasseresse and len(self.chasseresse) + len(cards) >= 3:
            return self.dragonCorail

        elif maintype == DragonCorail:
            return self.pearl

        elif maintype == Witch:
            return self.pearl + self.dragonPetrified

        elif maintype == Golem:
            return self.witch

        elif maintype == Guardian and len(self.guardian) + len(cards) >= 4:
            if len(self.guardian) + len(cards) > 4:
                return self.guardian + list(cards[0:4-len(self.guardian)])
            else:
                return self.guardian + list(cards)

        elif maintype == Horser and len(self.horser) + len(cards) >= 2:
            return self.witch + self.golem

        elif maintype == City and len(self.city) + len(cards) >= 3:
            return self.underTheBoard

        return []

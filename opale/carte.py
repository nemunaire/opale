#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 22:33:46 2020

@author: AlexandreMouchel
"""

class Card():

    def __init__(self, score):
        self.score = score

    def __repr__(self):
        return "\033[%dm%s\033[1m(%d)\033[0m"%(self.color, type(self).__name__, self.score)


class Chasseresse(Card):

    color = 91
    importance = 6

    def __init__(self, score):
        Card.__init__(self, score)


class DragonCorail(Card):

    color = 92
    importance = 7

    def __init__(self, score):
        Card.__init__(self, score)


class Pearl(Card):

    color = 93
    importance = 10

    def __init__(self, score):
        Card.__init__(self, score)


class Witch(Card):

    color = 94
    importance = 7

    def __init__(self, score):
        Card.__init__(self, score)


class DragonPetrified(Card):

    color = 95
    importance = 9

    def __init__(self, score):
        Card.__init__(self, score)


class Golem(Card):

    color = 91
    importance = 8

    def __init__(self, score):
        Card.__init__(self, score)


class Guardian(Card):

    color = 94
    importance = 5

    def __init__(self, score):
        Card.__init__(self, score)


class Horser(Card):

    color = 92
    importance = 7

    def __init__(self, score):
        Card.__init__(self, score)


class City(Card):

    color = 96
    importance = 6

    def __init__(self):
        Card.__init__(self, 0)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 22:20:15 2020

@author: AlexandreMouchel
"""

import random

from opale.carte import *
from opale.board import Board
from opale.computer import Computer
from opale.player import Player

class Game():

    def __init__(self, *player_names):
        self.board = Board()
        self.round = 0
        self.players = []

        for pname in player_names:
            if pname is not None:
                self.players.append(Player(pname))
            else:
                self.players.append(Computer(self.board))

    @property
    def player1(self):
        if type(self.players[0]) == Computer:
            return self.players[1]
        return self.players[0]

    @property
    def player2(self):
        if type(self.players[0]) == Computer:
            return self.players[0]
        return self.players[1]

    @property
    def current_player(self):
        return self.players[self.round % 2]

    @property
    def current_partner(self):
        return self.players[(self.round + 1) % 2]


    def ready(self):
        random.shuffle(self.players)


    def isFinished(self):
        return self.board.isFinished() or self.player1.cantpioche or self.player2.cantpioche

    def play_round(self, *cards):
        self.current_player.play_card(*cards)

        ret = self.board.play_card(*cards)

        for c in ret:
            if type(c) == DragonPetrified:
                self.current_partner.lose_dragon()
                self.current_player.take_dragon()
                break

        self.current_player.push_défausse(*ret)
        self.current_player.pick()

        self.round += 1

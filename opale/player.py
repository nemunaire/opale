#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 22:32:32 2020

@author: AlexandreMouchel
"""
import random

from opale.carte import *


class Player():

    def __init__(self, name):
        self.name = name
        self.pioche = []
        self.cantpioche = False
        self.défausse = []
        self.hand = []
        self.dragonPetrified = False

        # we are creating the deck
        self.pioche += [City() for i in range(6)]
        self.pioche += [Chasseresse(1 if i % 3 else 2) for i in range(6)]
        self.pioche += [DragonCorail(3 if i % 2 else i) for i in range(1, 5)]
        self.pioche += [Pearl((i+2)//2) for i in range(7)]
        self.pioche += [Witch(i%2+1) for i in range(4)]
        self.pioche += [DragonPetrified(2) for i in range(5)]
        self.pioche += [Golem(i+1) for i in range(3)]
        self.pioche += [Horser(2 if i == 0 else 1) for i in range(5)]
        self.pioche += [Guardian(1) for i in range(6)]
        self.pioche += [Guardian(2) for i in range(3)]
        self.pioche += [Guardian(3) for i in range(1)]
        random.shuffle(self.pioche)

        self.pick()

    def pick(self):
        for i in range((6 if self.dragonPetrified else 5) - len(self.hand)):
            if len(self.pioche) > 0:
                self.hand.append(self.pioche.pop())
            else:
                self.cantpioche = True

    def lose_dragon(self):
        self.dragonPetrified = False

        random.shuffle(self.hand)
        for i in range(len(self.hand)):
            self.pioche.append(self.hand.pop())

        self.pick()

    def take_dragon(self):
        self.dragonPetrified = True

    def push_défausse(self, *cards):
        self.défausse += cards

    def play_card(self, *cards):
        ret = []
        kind = None
        for card in cards:
            if kind is None:
                kind = type(card)
            elif kind != type(card):
                self.hand += ret
                raise Exception("You select multiple card types, this is not allowed. At least: %s and %s." % (kind.__name__, type(card).__name__))
            elif self.hand.count(card) <= 0:
                self.hand += ret
                raise Exception("You select too much cards %s than you have in your hand." % (kind.__name__))
            ret.append(card)
            self.hand.remove(card)
        return ret

    def get_score(self):
        score = 3 if self.dragonPetrified else 0
        for c in self.défausse:
            score += c.score
        return score

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from opale.game import Game

game = Game("PO", "A<3")

def play_turn():
    print("\n\n\n====================================================================\n")
    print("\033[1mCurrent player:\033[0m \033[93m%s\033[0m \033[91m%s\033[0m" % (game.current_player.name, "DRAGON" if game.current_player.dragonPetrified else ""))

    while True:
        print(game.board)
        print("\033[1mHere is your hand currently:\033[0m %s" % {i+1: game.current_player.hand[i] for i in range(len(game.current_player.hand))})
        icards = input("Choose one or more card to play: ")
        pcards = map(lambda x: int(x), icards.split(" "))
        good = True
        cards = []
        for card in pcards:
            if card <= 0 or card > len(game.current_player.hand):
                print("\033[91m%s is not a valid card.\033[0m" % card)
                good = False
                break
            else:
                cards.append(game.current_player.hand[card-1])

        if good:
            try:
                game.play_round(*cards)
                break
            except Exception as e:
                print("\033[91m%s\033[0m" % e)

while not game.isFinished():
    try:
        play_turn()
    except Exception as e:
        print("\033[91mSorry, something goes wrong: '%s'.\033[0m" % e)
        pass

if game.player1.cantpioche:
    print("\n\033[1;92m%s n'a plus de pioche ! Last round\033[0m" % game.player1.name)
elif game.player2.cantpioche:
    print("\n\033[1;92m%s n'a plus de pioche ! Last round\033[0m" % game.player2.name)
else:
    print("\n\033[1;92mCité sous-marine découverte ! Last round\033[0m")
input("Press enter to play last round! ")
play_turn()

print("\n")
print("\033[93m%s\033[0m: \033[1m%d\033[0m pts" % (game.current_player.name, game.current_player.get_score()))
print("\033[93m%s\033[0m: \033[1m%d\033[0m pts" % (game.current_partner.name, game.current_partner.get_score()))
